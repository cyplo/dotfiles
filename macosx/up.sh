#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" </dev/null
export PATH="/usr/local/bin:$PATH"

set +e
brew unlink gnupg
brew unlink gnupg2
brew uninstall gnupg
brew uninstall gnupg2
brew unlink dirmngr
brew uninstall dirmngr
brew unlink gpg-agent
brew uninstall gpg-agent
brew unlink mercurial
brew uninstall --force mercurial
brew unlink numpy
brew uninstall --ignore-dependencies numpy
brew unlink python
brew uninstall --ignore-dependencies python
brew unlink python@2
brew uninstall --ignore-dependencies python@2
brew install numpy
brew link --overwrite numpy
brew link --overwrite gcc
brew install --force dirmngr
set -e

brew update
brew install --force gnupg2
brew install python3
brew install mercurial
brew upgrade
brew cask upgrade

brew install reattach-to-user-namespace

brew install aria2
brew install \
  autojump \
  bash \
  mobile-shell \
  the_silver_searcher \
  tmux \
  tree \
  watch \
  zsh

brew install \
  coreutils \
  curl \
  dos2unix \
  findutils \
  gawk \
  gnu-sed \
  httpie \
  httping \
  jq \
  moreutils \
  pinentry \
  pinentry-mac \
  socat \
  unrar \
  wget \
  fontconfig \
  syncthing

brew install \
  carthage \
  cmake \
  elm \
  git \
  haskell-stack \
  mercurial \
  python \
  python3 \
  ruby \
  sqlite \
  tidy-html5 \
  doxygen \
  go \
  optipng \
  jpegoptim

brew cask install gimp
brew cask install iterm2
brew cask install docker
brew cask install appcode
brew cask install adium
brew cask install kdiff3
brew cask install istat-menus

echo "finsihed installing"

echo "restart syncthing"
reattach-to-user-namespace brew services restart syncthing

if ! fgrep /usr/local/bin/zsh /etc/shells; then
  echo "add shell"
  sudo bash -c "echo /usr/local/bin/zsh >> /etc/shells"
fi

echo "cleanup"
brew cleanup

echo "Configuring NVRAM"
sudo nvram SystemAudioVolume=%80
defaults write com.google.Keystone.Agent checkInterval 4233600

DIR="$DIR/../"
DIR=`realpath "$DIR"`

if [[ -z $CONTINUOUS_INTEGRATION ]]; then
    echo "Invoking common configuration scripts"
    DIR="$DIR" $DIR/common/up.sh
fi

ln -vfs "$DIR/.gitconfig.mac" $HOME/.gitconfig
mkdir -p "$HOME/Library/Application Support/Code/User/"
ln -vfs "$DIR/.config/Code/User/settings.json.mac" "$HOME/Library/Application Support/Code/User/settings.json"
ln -vfs "$DIR/.config/Code/User/keybindings.json.mac" "$HOME/Library/Application Support/Code/User/keybindings.json"
