{ config, pkgs, ... }:
{
  imports = [
    ./grobi.nix
    ./i3.nix
    ./dunst.nix
    ./rofi.nix
  ];

  home.sessionVariables = {
    CM_LAUNCHER="rofi";
  };

  services = {
    picom = {
      enable = true;
      vSync = true;
    };
    kdeconnect = {
      enable = true;
      indicator = true;
    };
    network-manager-applet.enable = true;
    pasystray.enable = true;
  };

  services.udiskie.enable = true;
  services.redshift.enable = true;
  services.redshift.provider = "geoclue2";

  xsession = {
    enable = true;
  };


}
